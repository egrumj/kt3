import java.util.*;

public class LongStack {

   private final LinkedList<Long> longs;
   private static final List<String> VALUES = Arrays.asList("+","-","/","*", "SWAP", "ROT");

   public static void main (String[] argum) {
      LongStack m = new LongStack();
      m.push(2);
      m.push(5);
      m.push(4);
      m.op("dasdasd");

   }

   LongStack() {
      this.longs = new LinkedList<>();
   }

   @Override
   public Object clone() throws CloneNotSupportedException {
      LongStack clo = new LongStack();
      for (Long l:longs) {
         clo.push(l);
      }
      return clo;
   }

   public boolean stEmpty() {
      return longs.isEmpty();
   }

   public void push (long a) {
      longs.addLast(a);
   }

   public long pop() {
      if (longs.size() > 0) {
         return longs.removeLast();
      }else {
         throw new RuntimeException("There is nothing to pop from the Stack.");
      }
   }

   public void op (String s) {
      long vim;
      long evim;
      try {
         vim = pop();
         evim = pop();
      } catch (RuntimeException e) {
         throw new RuntimeException("There are not enough elements to " + s);
      }
      if (VALUES.contains(s)) {
         if ("+".equals(s)) {
            push(evim + vim);
         } else if ("-".equals(s)) {
            push(evim - vim);
         } else if ("*".equals(s)) {
            push(evim * vim);
         } else if ("/".equals(s)) {
            push(evim / vim);
         } else if ("SWAP".equals(s)) {
            push(vim);
            push(evim);
         } else if ("ROT".equals(s)) {
            long third;
            try {
               third = pop();
               push(evim);
               push(vim);
               push(third);
            } catch (RuntimeException r) {
               throw new RuntimeException("There are not enough elemtns to ROT!");
            }
         }
      }else {
         throw new RuntimeException("Illegal symbol " + s + " in expression: " + evim + " " + s + " " + vim);
      }
   }
  
   public long tos() {
      if (longs.size() > 0) {
         return longs.get(longs.size() - 1);
      }else {
         throw new RuntimeException("LongStack is empty!");
      }
   }

   @Override
   public boolean equals (Object o) {
      return longs.equals(((LongStack)o).longs);
   }

   @Override
   public String toString() {
      StringBuilder str = new StringBuilder();
      for (Long l : longs) {
         str.append(l.toString());
      }
      return str.toString();
   }

   public static long interpret (String pol) {
      LongStack arr = new LongStack();
      String[] splited = null;
      int cN = 0;
      int cV = 0;
      try {
         splited = pol.split("[\t ]");
      }catch (NullPointerException e) {
         throw new RuntimeException("Expression can't be null!");
      }
      for (String element:splited) {
         if (element.equals("")) {
            continue;
         }
         if (numeric(element)){
            cN++;
            arr.push(Long.parseLong(element));
         }
         else if (VALUES.contains(element)) {
            if (!element.equals("SWAP") && !element.equals("ROT")) {
               cV++;
            }
            if (cV >= cN) {
               throw new RuntimeException("Can not perform " + element + " in expression: " + pol);
            }
            arr.op(element);
         }
         else {
            throw new RuntimeException("Illegal symbol " + element + " in expression: " + pol);
         }
      }
      if (arr.longs.size() == 0) {
         throw new RuntimeException("Empty expression!");
      }
      if (arr.longs.size() > 1) {
         throw new RuntimeException("Too many numbers in expression: " + pol);
      }
      return arr.tos();
   }

   public static boolean numeric (String str) {
      boolean numeric = true;
      try {
         Long l = Long.parseLong(str);
      } catch (NumberFormatException e) {
         numeric = false;
      }
      return numeric;
   }

}

